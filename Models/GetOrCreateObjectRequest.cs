﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActiveDirectorySyncModule.Models
{
    public class GetOrCreateObjectRequest
    {
        public string IdObject { get; set; }
        public string NameObject { get; set; }
        public string IdClass { get; set; }
        public bool UseExisting { get; set; } = true;
    }
}
