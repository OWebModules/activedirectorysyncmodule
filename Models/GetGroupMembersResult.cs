﻿using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActiveDirectorySyncModule.Models
{
    public class GetGroupMembersResult
    {
        public List<ResultItem<GroupWithMembers>> GroupMembers { get; set; } = new List<ResultItem<GroupWithMembers>>();
    }

    public class GroupWithMembers
    {
        public GroupPrincipal GroupPrincipal { get; set; }
        public List<UserItem> UserPrincipals { get; set; } = new List<UserItem>();
    }
}
