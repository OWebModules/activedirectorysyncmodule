﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActiveDirectorySyncModule.Models
{
    public class ExportGroupMembersCSVRequest
    {
        public List<GroupWithMembers> GroupWithMembers { get; private set; } = new List<GroupWithMembers>();
        public string CSVPath { get; private set; }

        public IMessageOutput MessageOutput { get; set; }

        public ExportGroupMembersCSVRequest(List<GroupWithMembers> groupWithMembers, string csvPath)
        {
            GroupWithMembers = groupWithMembers;
            CSVPath = csvPath;
        }
    }
}
