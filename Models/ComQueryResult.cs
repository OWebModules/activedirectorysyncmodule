﻿using OntologyClasses.BaseClasses;
using PartnerModule.Models;
using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActiveDirectorySyncModule.Models
{
    public class ComQueryResult
    {
        public List<clsOntologyItem> UserList { get; set; } = new List<clsOntologyItem>();
        public List<Partner> PartnerList { get; set; } = new List<Partner>();

        public List<PrincipalItem> UserPrincipalList { get; set; } = new List<PrincipalItem>();
    }

    public class PrincipalItem
    {
        public clsOntologyItem User { get; set; }
        public Partner Partner { get; set; }

        public UserPrincipal UserPrincipal { get; set; }
    }
}
