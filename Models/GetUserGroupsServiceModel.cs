﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActiveDirectorySyncModule.Models
{
    public class GetUserGroupsServiceModel
    {
        public clsOntologyItem RootConfig { get; set; }
        public List<clsOntologyItem> Configs { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> ConfigsToExportPaths { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ConfigsToUsers { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> PrincipalNamesToUsers { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> PrincipalPathsToUsers { get; set; } = new List<clsObjectRel>();

    }
}
