﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActiveDirectorySyncModule.Models
{
    public class CompareGroupMembersRequest
    {
        public List<GroupWithMembers> GroupMembersSrc { get; private set; } = new List<GroupWithMembers>();
        public List<GroupWithMembers> GroupMembersDst { get; private set; } = new List<GroupWithMembers>();
        public IMessageOutput MessageOutput { get; set; }

        public CompareGroupMembersRequest(List<GroupWithMembers> srcGroupMembers, List<GroupWithMembers> dstGroupMembers)
        {
            GroupMembersSrc = srcGroupMembers;
            GroupMembersDst = dstGroupMembers;
        }
    }
}
