﻿using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActiveDirectorySyncModule.Models
{
    public class GetUserGroupsResult
    {
        public clsOntologyItem User { get; set; }
        public List<GroupItem> Groups { get; set; } = new List<GroupItem>();
    }

    
}
