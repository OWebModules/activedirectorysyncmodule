﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActiveDirectorySyncModule.Models
{
    public class UserItem
    {
        public string Id { get; private set; }
        public string Name { get; private set; }
        public string DistinguishedName { get; private set; }
        public string SamAccountName { get; private set; }

        public UserItem(Principal principal)
        {
            Id = principal.Guid.ToString();
            Name = principal.Name;
            DistinguishedName = principal.DistinguishedName;
            SamAccountName = principal.SamAccountName;
        }
    }
}
