﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ActiveDirectorySyncModule.Models
{
    public class GetUserGroupsRequest
    { 
        public string IdConfig { get; private set; }
        public IMessageOutput MessageOutput { get; set; }

        public GetUserGroupsRequest(string idConfig)
        {
            IdConfig = idConfig;
        }
    }
}
