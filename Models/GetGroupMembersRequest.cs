﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ActiveDirectorySyncModule.Models
{
    public class GetGroupMembersRequest
    {
        public string GroupFilter { get; private set; }
        public string OuPrincipalPath { get; private set; }
        public string OuPrincipalName { get; private set; }
        public Regex GroupRegex { get; private set; }
        public IMessageOutput MessageOutput { get; set; }

        public clsOntologyItem CheckAndSetGroupRegex(Globals globals)
        {
            var result = globals.LState_Success.Clone();
            try
            {
                GroupRegex = new Regex(GroupFilter);
            }
            catch (Exception ex)
            {
                result.Additional1 = ex.Message;
            }

            return result;
        }

        public GetGroupMembersRequest(string groupFilter, string ouPath, string ouName)
        {
            GroupFilter = groupFilter;
            OuPrincipalPath = ouPath;
            OuPrincipalName = ouName;
        }
    }
}
