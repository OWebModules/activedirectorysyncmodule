﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActiveDirectorySyncModule.Models
{
    public class CompareGroupMembersResult
    {
        public List<UserItem> InSrc { get; set; } = new List<UserItem>();
        public List<UserItem> InDst { get; set; } = new List<UserItem>();
        public List<GroupItem> SrcGroups { get; set; } = new List<GroupItem>();
        public List<GroupItem> DstGroups { get; set; } = new List<GroupItem>();
        public List<string> SrcDistinguishedNames { get; set; } = new List<string>();
        public List<string> DstDistinguishedNames { get; set; } = new List<string>();

        public List<UserItem> InSrcNotInDst { get; set; } = new List<UserItem>();
        public List<UserItem> InDstNotInSrc { get; set; } = new List<UserItem>();
    }
}
