﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActiveDirectorySyncModule.Models
{
    public class GetModelResult
    {
        public List<clsOntologyItem> UserList { get; set; } = new List<clsOntologyItem>();
        public List<clsOntologyItem> PartnerList { get; set; } = new List<clsOntologyItem>();


    }
}
