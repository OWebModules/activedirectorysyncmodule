﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActiveDirectorySyncModule.Models
{
    public class ComQueryRequest
    {
        public List<string> IdUsers { get; set; } = new List<string>();
        public List<string> IdPartners { get; set; } = new List<string>();

        public bool AllPartners { get; set; } = false;

        public IMessageOutput MessageOutput { get; set; }
    }
}
