﻿using ActiveDirectorySyncModule.Models;
using ActiveDirectorySyncModule.Validation;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActiveDirectorySyncModule.Services
{
    public class ElasticAgent : ElasticBaseAgent
    {

        public async Task<ResultItem<GetUserGroupsServiceModel>> GetUserGroupsModel(GetUserGroupsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetUserGroupsServiceModel>>(() =>
           {
               var result = new ResultItem<GetUserGroupsServiceModel>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new GetUserGroupsServiceModel()
               };

               result.ResultState = ValidationController.ValidateGetUserMembershipRequest(request, globals);
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchRootConfig = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID = request.IdConfig,
                       GUID_Parent = GetUserGroups.Config.LocalData.Class_Get_User_Groups.GUID
                   }
               };

               var dbReaderRootConfig = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderRootConfig.GetDataObjects(searchRootConfig);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Root-Config!";
                   return result;
               }

               result.Result.RootConfig = dbReaderRootConfig.Objects1.FirstOrDefault();

               result.ResultState = ValidationController.ValidateGetUserGroupsModel(result.Result, globals, nameof(GetUserGroupsServiceModel.RootConfig));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchConfigs = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.RootConfig.GUID,
                       ID_RelationType = GetUserGroups.Config.LocalData.ClassRel_Get_User_Groups_contains_Get_User_Groups.ID_RelationType,
                       ID_Parent_Other = GetUserGroups.Config.LocalData.ClassRel_Get_User_Groups_contains_Get_User_Groups.ID_Class_Right
                   }
               };

               var dbReaderConfigs = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderConfigs.GetDataObjectRel(searchConfigs);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the sub-configs!";
                   return result;
               }

               result.Result.Configs = dbReaderConfigs.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).ToList();

               if (!result.Result.Configs.Any())
               {
                   result.Result.Configs.Add(result.Result.RootConfig);
               }

               var searchPaths = result.Result.Configs.Select(config => new clsObjectRel
               {
                   ID_Object = config.GUID,
                   ID_RelationType = GetUserGroups.Config.LocalData.ClassRel_Get_User_Groups_export_to_Path.ID_RelationType,
                   ID_Parent_Other = GetUserGroups.Config.LocalData.ClassRel_Get_User_Groups_export_to_Path.ID_Class_Right
               }).ToList();

               var dbReaderPaths = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderPaths.GetDataObjectRel(searchPaths);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Export-path!";
                   return result;
               }

               result.Result.ConfigsToExportPaths = dbReaderPaths.ObjectRels;

               var searchUsers = result.Result.Configs.Select(cfg => new clsObjectRel
               {
                   ID_Object = cfg.GUID,
                   ID_RelationType = GetUserGroups.Config.LocalData.ClassRel_Get_User_Groups_belonging_user.ID_RelationType,
                   ID_Parent_Other = GetUserGroups.Config.LocalData.ClassRel_Get_User_Groups_belonging_user.ID_Class_Right
               }).ToList();

               var dbReaderUsers = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderUsers.GetDataObjectRel(searchUsers);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the users!";
                   return result;
               }

               result.Result.ConfigsToUsers = dbReaderUsers.ObjectRels;

               result.ResultState = ValidationController.ValidateGetUserGroupsModel(result.Result, globals, nameof(GetUserGroupsServiceModel.ConfigsToUsers));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchPrincipalPaths = result.Result.ConfigsToUsers.Select(rel => new clsObjectRel
               {
                   ID_Other = rel.ID_Other,
                   ID_RelationType = GetUserGroups.Config.LocalData.ClassRel_Principal_Path_belongs_to_user.ID_RelationType,
                   ID_Parent_Object = GetUserGroups.Config.LocalData.ClassRel_Principal_Path_belongs_to_user.ID_Class_Left
               }).ToList();

               var dbReaderPrincipalPaths = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderPrincipalPaths.GetDataObjectRel(searchPrincipalPaths);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Principal-Paths!";
                   return result;
               }

               result.Result.PrincipalPathsToUsers = dbReaderPrincipalPaths.ObjectRels;

               result.ResultState = ValidationController.ValidateGetUserGroupsModel(result.Result, globals, nameof(GetUserGroupsServiceModel.PrincipalPathsToUsers));
               
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchPrincipalNames = result.Result.ConfigsToUsers.Select(rel => new clsObjectRel
               {
                   ID_Other = rel.ID_Other,
                   ID_RelationType = GetUserGroups.Config.LocalData.ClassRel_Principal_Name_belongs_to_user.ID_RelationType,
                   ID_Parent_Object = GetUserGroups.Config.LocalData.ClassRel_Principal_Name_belongs_to_user.ID_Class_Left
               }).ToList();

               var dbReaderPrincipalNames = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderPrincipalNames.GetDataObjectRel(searchPrincipalNames);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Principal-Names!";
                   return result;
               }

               result.Result.PrincipalNamesToUsers = dbReaderPrincipalNames.ObjectRels;

               result.ResultState = ValidationController.ValidateGetUserGroupsModel(result.Result, globals, nameof(GetUserGroupsServiceModel.PrincipalNamesToUsers));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               return result;
           });

            return taskResult;
        }

        public async Task<clsOntologyItem> CheckUserGroups(List<GetUserGroupsResult> getUserGroupsResult)
        {
            var taskResult = await Task.Run<clsOntologyItem>(async() =>
            {
                var result = globals.LState_Success.Clone();
                
                var searchUserGroups = getUserGroupsResult.Select(userGroup => new clsObjectRel
                {
                    ID_Other = userGroup.User.GUID,
                    ID_RelationType = GetUserGroups.Config.LocalData.ClassRel_Group_contains_user.ID_RelationType,
                    ID_Parent_Object = GetUserGroups.Config.LocalData.ClassRel_Group_contains_user.ID_Class_Left
                }).ToList();

                var dbReaderUserGroups = new OntologyModDBConnector(globals);

                result = dbReaderUserGroups.GetDataObjectRel(searchUserGroups);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while getting the Groups of Users!";
                    return result;
                }

                var toAdd = new List<clsObjectRel>();
                var toDelete = new List<clsObjectRel>();
                var newGroups = new List<GroupItem>();
                foreach (var userGroup in getUserGroupsResult)
                {
                    var groupsInDb = dbReaderUserGroups.ObjectRels.Where(rel => rel.ID_Other == userGroup.User.GUID).ToList();
                    toDelete.AddRange(from groupInDb in groupsInDb
                                join groupOfUser in userGroup.Groups on groupInDb.Name_Object equals groupOfUser.Name into groupOfUsers
                                from groupOfUser in groupOfUsers.DefaultIfEmpty()
                                where groupOfUser == null
                                select new clsObjectRel
                                {
                                    ID_Object = groupInDb.ID_Object,
                                    ID_RelationType = groupInDb.ID_RelationType,
                                    ID_Other = groupInDb.ID_Other
                                });
                    newGroups.AddRange(from groupOfUser in userGroup.Groups
                                     join groupInDb in groupsInDb on groupOfUser.Name equals groupInDb.Name_Object into groupsInDb1
                                     from groupInDb in groupsInDb1.DefaultIfEmpty()
                                     where groupInDb == null
                                     select groupOfUser);


                }

                var groupsToCheck = newGroups.GroupBy(userGrp => userGrp.Name).Select(grp => new clsOntologyItem { Name = grp.Key, GUID_Parent = GetUserGroups.Config.LocalData.Class_Group.GUID }).ToList();

                var checkedObjectsResult = await CheckObjects(groupsToCheck, GetUserGroups.Config.LocalData.Class_Group.GUID);
                result = checkedObjectsResult.ResultState;
                if (result.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var relationConfig = new clsRelationConfig(globals);
                foreach (var userGroup in getUserGroupsResult)
                {
                    toAdd.AddRange(from groupToRelate in userGroup.Groups
                                   join groupChecked in checkedObjectsResult.Result on groupToRelate.Name equals groupChecked.Name
                                   select relationConfig.Rel_ObjectRelation(groupChecked, userGroup.User, GetUserGroups.Config.LocalData.RelationType_contains));
                }

                var dbWriter = new OntologyModDBConnector(globals);
                if (toDelete.Any())
                {
                    result = dbWriter.DelObjectRels(toDelete);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saveing the relations!";
                        return result;
                    }
                }

                if (toAdd.Any())
                {
                    result = dbWriter.SaveObjRel(toAdd);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while adding the relations!";
                        return result;
                    }
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<clsOntologyItem>> GetOrCreateObject(GetOrCreateObjectRequest request)
        {
            var taskResult = await Task.Run<ResultItem<clsOntologyItem>>(() =>
            {
                var result = new ResultItem<clsOntologyItem>
                {
                    ResultState = globals.LState_Success.Clone()
                };
                var dbReaderObject = new OntologyModDBConnector(globals);

                if (!string.IsNullOrEmpty( request.IdObject) || (!string.IsNullOrEmpty(request.NameObject) && !string.IsNullOrEmpty(request.IdClass)))
                {

                    var searchObject = new List<clsOntologyItem>
                    {
                        new clsOntologyItem
                        {
                            GUID = request.IdObject,
                            Name = request.NameObject,
                            GUID_Parent = request.IdClass
                        }
                    };
                    

                    result.ResultState = dbReaderObject.GetDataObjects(searchObject);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while reading object";
                        return result;
                    }

                    if (dbReaderObject.Objects1.Count == 1)
                    {
                        result.Result = dbReaderObject.Objects1.FirstOrDefault();

                        if (result.Result != null)
                        {
                            return result;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(request.NameObject) && request.UseExisting)
                        {
                            var objects = dbReaderObject.Objects1.Where(obj => obj.Name.ToLower() == request.NameObject.ToLower());

                            if (objects.Any())
                            {
                                result.Result = objects.First();
                                return result;
                            }
                        }
                    }
                    
                    
                }


                if (string.IsNullOrEmpty(request.IdClass))
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No idClass provided";
                    return result;
                }

                
                var searchClass = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdClass
                    }
                };

                var dbReaderClass = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderClass.GetDataClasses(searchClass);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Class";
                    return result;
                }

                if (!dbReaderClass.Classes1.Any())
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "Class is not existing";
                    return result;
                }

                var transaction = new clsTransaction(globals);

                var objectToSave = new clsOntologyItem
                {
                    GUID = !string.IsNullOrEmpty(request.IdObject) ? request.IdObject : globals.NewGUID,
                    Name = request.NameObject,
                    GUID_Parent = request.IdClass,
                    Type = globals.Type_Object
                };

                result.ResultState = transaction.do_Transaction(objectToSave);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while saving Object";
                    return result;
                }

                result.Result = objectToSave;

                
                return result;
            });

            return taskResult;
        }
        public async Task<ResultItem<GetModelResult>> GetModel(ComQueryRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetModelResult>>(() =>
            {
                var result = new ResultItem<GetModelResult>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new GetModelResult()
                };

                if (request.AllPartners)
                {

                }
                else
                {
                    if (!request.IdUsers.Any() && !request.IdPartners.Any())
                    {
                        result.ResultState = globals.LState_Error.Clone();
                        result.ResultState.Additional1 = "No User-Ids or Partner-Ids provided!";
                        return result;
                    }
                }
                

                if (!request.AllPartners)
                {
                    var searchUsers = request.IdUsers.Select(id => new clsOntologyItem
                    {
                        GUID = id,
                        GUID_Parent = Config.LocalData.Class_user.GUID
                    }).ToList();

                    if (searchUsers.Any())
                    {
                        var dbReaderUsers = new OntologyModDBConnector(globals);

                        result.ResultState = dbReaderUsers.GetDataObjects(searchUsers);

                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while reading users";
                            return result;
                        }
                        result.Result.UserList = dbReaderUsers.Objects1;
                    }
                }


                var searchPartners = new List<clsOntologyItem>();

                if (request.AllPartners)
                {
                    searchPartners = new List<clsOntologyItem>
                    {
                        new clsOntologyItem
                        {
                            GUID_Parent = Config.LocalData.Class_Partner.GUID
                        }
                    };
                }
                else
                {
                    searchPartners = request.IdPartners.Select(id => new clsOntologyItem
                    {
                        GUID = id,
                        GUID_Parent = Config.LocalData.Class_Partner.GUID
                    }).ToList();
                }
                

                if (searchPartners.Any())
                {
                    var dbReaderPartners = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderPartners.GetDataObjects(searchPartners);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while reading Partners";
                        return result;
                    }
                    result.Result.PartnerList = dbReaderPartners.Objects1;
                }

                if (!result.Result.UserList.Any() && !result.Result.PartnerList.Any())
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Users and no Partners found!";
                    return result;
                }

                return result;
            });
            return taskResult;
        }

        public ElasticAgent(Globals globals) : base(globals)
        {
            this.globals = globals;
        }
    }
}
