﻿using ActiveDirectorySyncModule.Models;
using ActiveDirectorySyncModule.Services;
using ActiveDirectorySyncModule.Validation;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using PartnerModule.Connectors;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActiveDirectorySyncModule
{
    public class ActiveDirectorySyncController : AppController
    {

        public async Task<clsOntologyItem> UpdatePartners(ComQueryRequest request)
        {
            var queryResult = await QueryActiveDirectory(request);

            var result = Globals.LState_Success.Clone();

            result = queryResult.ResultState;
            if (result.GUID == Globals.LState_Error.GUID)
            {
                return result;
            }

            result = await UpdatePartners(queryResult.Result);

            return result;
        }

        public async Task<clsOntologyItem> UpdatePartners(ComQueryResult queryResult)
        {
            var taskResult = await Task.Run<clsOntologyItem>(async () =>
            {
                var result = Globals.LState_Success.Clone();

                var partnerConnector = new PartnerConnector(Globals);
                var elasticAgent = new ElasticAgent(Globals);
                var relationConfig = new clsRelationConfig(Globals);
                var transaction = new clsTransaction(Globals);

                foreach (var item in queryResult.UserPrincipalList)
                {
                    if (!string.IsNullOrEmpty(item.UserPrincipal.EmailAddress))
                    {
                        var oItemResult = await elasticAgent.GetOrCreateObject(new GetOrCreateObjectRequest
                        {
                            IdClass = partnerConnector.LocalConfig.OItem_type_email_address.GUID,
                            NameObject = item.UserPrincipal.EmailAddress
                        });

                        result = oItemResult.ResultState;

                        if (result.GUID == Globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        if (item.Partner.CommunicationData == null || !item.Partner.CommunicationData.EmailAddress.Any())
                        {
                            var saveResult = await partnerConnector.SaveCommunicationItem(oItemResult.Result, item.Partner, partnerConnector.LocalConfig.OItem_relationtype_contains, ComItemType.Email);

                            result = saveResult.ResultState;
                            if (result.GUID == Globals.LState_Error.GUID)
                            {
                                result.Additional1 = $"Error while saveing Emailadress of Partner {item.Partner.Name}";
                                return result;
                            }
                        }
                        else
                        {
                            if (!item.Partner.CommunicationData.EmailAddress.Any(eAdr => eAdr.Name == item.UserPrincipal.EmailAddress))
                            {
                                var rel = relationConfig.Rel_ObjectRelation(new clsOntologyItem
                                {
                                    GUID = item.Partner.CommunicationData.Id,
                                    Name = item.Partner.CommunicationData.Name,
                                    GUID_Parent = partnerConnector.LocalConfig.OItem_type_kommunikationsangaben.GUID,
                                    Type = Globals.Type_Object
                                }, oItemResult.Result, partnerConnector.LocalConfig.OItem_relationtype_contains);

                                transaction.ClearItems();
                                result = transaction.do_Transaction(rel, true);
                                if (result.GUID == Globals.LState_Error.GUID)
                                {
                                    result.Additional1 = "Error while saving relation between Kommunikationsangaben and Emailadress";
                                    return result;
                                }
                            }

                        }

                    }
                    if (!string.IsNullOrEmpty(item.UserPrincipal.VoiceTelephoneNumber))
                    {
                        var oItemResult = await elasticAgent.GetOrCreateObject(new GetOrCreateObjectRequest
                        {
                            IdClass = partnerConnector.LocalConfig.OItem_type_telefonnummer.GUID,
                            NameObject = item.UserPrincipal.VoiceTelephoneNumber
                        });

                        result = oItemResult.ResultState;

                        if (result.GUID == Globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        if (item.Partner.CommunicationData == null || item.Partner.CommunicationData.PhoneNumbers == null || !item.Partner.CommunicationData.PhoneNumbers.Any())
                        {
                            var saveResult = await partnerConnector.SaveCommunicationItem(oItemResult.Result, item.Partner, partnerConnector.LocalConfig.OItem_relationtype_tel, ComItemType.Tel);

                            result = saveResult.ResultState;
                            if (result.GUID == Globals.LState_Error.GUID)
                            {
                                result.Additional1 = $"Error while saving Phonenumber of Partner {item.Partner.Name}";
                                return result;
                            }
                        }
                        else
                        {
                            if (!item.Partner.CommunicationData.PhoneNumbers.Any(eAdr => eAdr.Name == item.UserPrincipal.VoiceTelephoneNumber))
                            {
                                var rel = relationConfig.Rel_ObjectRelation(new clsOntologyItem
                                {
                                    GUID = item.Partner.CommunicationData.Id,
                                    Name = item.Partner.CommunicationData.Name,
                                    GUID_Parent = partnerConnector.LocalConfig.OItem_type_kommunikationsangaben.GUID,
                                    Type = Globals.Type_Object
                                }, oItemResult.Result, partnerConnector.LocalConfig.OItem_relationtype_tel);

                                transaction.ClearItems();
                                result = transaction.do_Transaction(rel, true);
                                if (result.GUID == Globals.LState_Error.GUID)
                                {
                                    result.Additional1 = "Error while saving relation between Kommunikationsangaben and Phonenumber";
                                    return result;
                                }
                            }

                        }

                    }


                }

                return result;
            });

            return taskResult;
        }
        public async Task<ResultItem<ComQueryResult>> QueryActiveDirectory(ComQueryRequest request)
        {
            var elasticAgent = new ElasticAgent(Globals);

            var taskResult = await Task.Run<ResultItem<ComQueryResult>>(async () =>
            {
                request.MessageOutput?.OutputInfo("Get model...");
                var modelResult = await elasticAgent.GetModel(request);
                request.MessageOutput?.OutputInfo("Have model.");

                var result = new ResultItem<ComQueryResult>
                {
                    ResultState = modelResult.ResultState,
                    Result = new ComQueryResult()
                };

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                result.Result.UserList = modelResult.Result.UserList;


                var partnerConnector = new PartnerConnector(Globals);
                if (modelResult.Result.PartnerList.Any())
                {

                    request.MessageOutput?.OutputInfo("Get partners...");
                    foreach (var partnerItm in modelResult.Result.PartnerList)
                    {
                        var partnerResult = await partnerConnector.GetPartnerData(partnerItm);
                        result.ResultState = partnerResult.ResultState;
                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while getting Partner";
                            request.MessageOutput?.OutputError(result.ResultState.Additional1);
                            return result;
                        }
                        result.Result.PartnerList.Add(partnerResult.Result);
                    }

                    request.MessageOutput?.OutputInfo($"Have {result.Result.PartnerList.Count} Partners");

                }

                var queryablePartnerList = result.Result.PartnerList.Where(part =>
                    (part.PersonalData != null && (!string.IsNullOrEmpty(part.PersonalData.NameVorname) || !string.IsNullOrEmpty(part.PersonalData.NameNachname)) ||
                    (part.CommunicationData != null && (part.CommunicationData.EmailAddress.Any()))));

                if (!queryablePartnerList.Any() && !result.Result.UserList.Any())
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Partner with Vorname or Nachname or no User given";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo($"Have {queryablePartnerList.Count()} queryable Partners");

                var ctx = new PrincipalContext(ContextType.Domain);
                UserPrincipal userToSearch = new UserPrincipal(ctx);

                var principalSearcher = new PrincipalSearcher(userToSearch);
                var wholeList = principalSearcher.FindAll().Cast<UserPrincipal>();

                result.Result.UserPrincipalList.AddRange(from principal in wholeList
                                                         from partner in result.Result.PartnerList.Where(partner => partner.CommunicationData == null || !partner.CommunicationData.EmailAddress.Any())
                                                         where !string.IsNullOrEmpty(principal.GivenName) && !string.IsNullOrEmpty(principal.Surname) && partner.PersonalData != null && principal.GivenName == partner.PersonalData.NameVorname && principal.Surname == partner.PersonalData.NameNachname
                                                         select new PrincipalItem
                                                         {
                                                             Partner = partner,
                                                             UserPrincipal = principal
                                                         });

                result.Result.UserPrincipalList.AddRange(from principal in wholeList
                                                         from partner in result.Result.PartnerList.Where(partner => partner.CommunicationData != null && partner.CommunicationData.EmailAddress.Any())
                                                         where partner.CommunicationData != null && partner.CommunicationData.EmailAddress.Any(eAdr => eAdr.Name == principal.EmailAddress)
                                                         select new PrincipalItem
                                                         {
                                                             Partner = partner,
                                                             UserPrincipal = principal
                                                         });


                result.Result.UserPrincipalList.AddRange(from principal in wholeList
                                                         from user in result.Result.UserList
                                                         where principal.SamAccountName == user.Name
                                                         select new PrincipalItem
                                                         {
                                                             User = user,
                                                             UserPrincipal = principal
                                                         });

                request.MessageOutput?.OutputInfo($"Done {result.Result.UserPrincipalList.Count} User Principals.");
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<GetGroupMembersResult>> GetGroupMembers(GetGroupMembersRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetGroupMembersResult>>(() =>
           {
               var result = new ResultItem<GetGroupMembersResult>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new GetGroupMembersResult()
               };

               request.MessageOutput?.OutputInfo("Validate request...");
               result.ResultState = ValidationController.ValidateGetGroupMembersRequest(request, Globals);

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Validated request.");

               request.MessageOutput?.OutputInfo("Get Context and PrincipalSearcher...");
               var ctx = new PrincipalContext(ContextType.Domain, request.OuPrincipalName, request.OuPrincipalPath);
               var findAllGroups = new GroupPrincipal(ctx, "*");

               PrincipalSearcher ps = new PrincipalSearcher(findAllGroups);

               request.MessageOutput?.OutputInfo($"Have Context {ctx.Name} / User {ctx.UserName} and PrincipalSearcher with Filter {ps.QueryFilter }");
               request.MessageOutput?.OutputInfo("Search Groups and Members...");
               foreach (var adItem in ps.FindAll())
               {
                   var adGroup = adItem as GroupPrincipal;

                   if (adGroup != null && request.GroupRegex.IsMatch(adGroup.Name))
                   {
                       var groupWithMember = new GroupWithMembers();
                       groupWithMember.GroupPrincipal = adGroup;
                       var resultItem = new ResultItem<GroupWithMembers>
                       {
                           ResultState = Globals.LState_Success.Clone(),
                           Result = groupWithMember
                       };

                       result.Result.GroupMembers.Add(resultItem);
                       try
                       {
                           var searchResult = adGroup.GetMembers();
                           groupWithMember.UserPrincipals = searchResult.Select(usr => new UserItem(usr)).ToList();
                       }
                       catch (Exception ex)
                       {
                           resultItem.ResultState = Globals.LState_Error.Clone();
                           resultItem.ResultState.Additional1 = ex.Message;
                       }
                   }


               }
               request.MessageOutput?.OutputInfo("Found Groups and Members.");
               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<List<GetUserGroupsResult>>> GetUserGroups(GetUserGroupsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<List<GetUserGroupsResult>>>(async () =>
            {
                var result = new ResultItem<List<GetUserGroupsResult>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new List<GetUserGroupsResult>()
                };

                request.MessageOutput?.OutputInfo("Validate request...");
                result.ResultState = ValidationController.ValidateGetUserMembershipRequest(request, Globals);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Validated request.");

                request.MessageOutput?.OutputInfo("Get model...");
                var elasticAgent = new Services.ElasticAgent(Globals);
                var modelResult = await elasticAgent.GetUserGroupsModel(request);

                result.ResultState = modelResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Have model.");

                var exportPath = modelResult.Result.ConfigsToExportPaths.FirstOrDefault();

                request.MessageOutput?.OutputInfo($"{modelResult.Result.Configs.Count} Configs.");
                foreach (var config in modelResult.Result.Configs)
                {
                    request.MessageOutput?.OutputInfo($"Config {config.Name}");
                    var principalInfos = (from user in modelResult.Result.ConfigsToUsers.Where(cfgToUser => cfgToUser.ID_Object == config.GUID)
                                          join principalName in modelResult.Result.PrincipalNamesToUsers on user.ID_Other equals principalName.ID_Other
                                          join principalPath in modelResult.Result.PrincipalPathsToUsers on user.ID_Other equals principalPath.ID_Other
                                          select new { user, principalName, principalPath }).FirstOrDefault();

                    var resultItem = new GetUserGroupsResult
                    {
                        User = new clsOntologyItem
                        {
                            GUID = principalInfos.user.ID_Other,
                            Name = principalInfos.user.Name_Other,
                            GUID_Parent = principalInfos.user.ID_Parent_Other,
                            Type = principalInfos.user.Ontology
                        }
                    };

                    result.Result.Add(resultItem);

                    try
                    {
                        var ctx = new PrincipalContext(ContextType.Domain, principalInfos.principalName.Name_Object, principalInfos.principalPath.Name_Object);
                        var findUser = UserPrincipal.FindByIdentity(ctx, principalInfos.user.Name_Other);
                        if (findUser == null)
                        {
                            result.ResultState = Globals.LState_Error.Clone();
                            result.ResultState.Additional1 = "User cannot be found!";
                            request.MessageOutput?.OutputError(result.ResultState.Additional1);
                            return result;
                        }
                        var groupsResult = findUser.GetGroups();
                        resultItem.Groups.AddRange(groupsResult.Select(grp => new GroupItem(grp)));
                    }
                    catch (Exception ex)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = ex.Message;
                    }
                }

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Save new groups, relations and delete old group-relations...");
                result.ResultState = await elasticAgent.CheckUserGroups(result.Result);
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Saved new groups, relations and delete old group-relations.");

                if (exportPath != null)
                {
                    try
                    {
                        request.MessageOutput?.OutputInfo($"Export to path {exportPath.Name_Other}...");
                        var jsonToExport = Newtonsoft.Json.JsonConvert.SerializeObject(result.Result);
                        if (File.Exists(exportPath.Name_Other))
                        {
                            request.MessageOutput?.OutputInfo($"File exists. Delete it.");
                            File.Delete(exportPath.Name_Other);
                        }
                        File.WriteAllText(exportPath.Name_Other, jsonToExport);
                        request.MessageOutput?.OutputInfo($"Exported file.");

                    }
                    catch (Exception ex)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = ex.Message;
                    }
                }

                return result;
            });

            return taskResult;
        }
        public async Task<clsOntologyItem> ExportGroupMembersToCSV(ExportGroupMembersCSVRequest request)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = Globals.LState_Success.Clone();

                request.MessageOutput?.OutputInfo("Validate request...");
                result = ValidationController.ValidateExportGroupMembersCSVRequest(request, Globals);

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Validated request.");

                try
                {
                    request.MessageOutput?.OutputInfo($"Open File {request.CSVPath}");
                    using (var textWriter = new StreamWriter(request.CSVPath, false, Encoding.UTF8))
                    {
                        textWriter.WriteLine("GroupName;GroupPath;UserName;UserPath");

                        request.MessageOutput?.OutputInfo($"Write {request.GroupWithMembers.Count} Groups and {request.GroupWithMembers.SelectMany(grp => grp.UserPrincipals).Count()} Members...");
                        foreach (var groupWithMembers in request.GroupWithMembers)
                        {
                            foreach (var userPrincipal in groupWithMembers.UserPrincipals)
                            {
                                textWriter.WriteLine($"{groupWithMembers.GroupPrincipal.Name};{groupWithMembers.GroupPrincipal.DistinguishedName};{userPrincipal.Name};{userPrincipal.DistinguishedName}");
                            }
                        }
                    }

                    request.MessageOutput?.OutputInfo($"Written groups and members.");

                }
                catch (Exception ex)
                {
                    result = Globals.LState_Error.Clone();
                    result.Additional1 = ex.Message;
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<CompareGroupMembersResult>> CompareGroupMembers(CompareGroupMembersRequest request)
        {
            var taskResult = await Task.Run<ResultItem<CompareGroupMembersResult>>(() =>
           {
               var result = new ResultItem<CompareGroupMembersResult>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new CompareGroupMembersResult()
               };

               result.Result.InSrc = request.GroupMembersSrc.SelectMany(grp => grp.UserPrincipals).ToList();
               result.Result.InDst = request.GroupMembersDst.SelectMany(grp => grp.UserPrincipals).ToList();
               result.Result.SrcDistinguishedNames = request.GroupMembersSrc.SelectMany(grp => grp.UserPrincipals).GroupBy(grp => grp.DistinguishedName).Select(grp => grp.Key).ToList();
               result.Result.DstDistinguishedNames = request.GroupMembersDst.SelectMany(grp => grp.UserPrincipals).GroupBy(grp => grp.DistinguishedName).Select(grp => grp.Key).ToList();
               result.Result.SrcGroups = request.GroupMembersSrc.Select(grp => grp.GroupPrincipal).Select(grp => new GroupItem(grp)).ToList();
               result.Result.DstGroups = request.GroupMembersDst.Select(grp => grp.GroupPrincipal).Select(grp => new GroupItem(grp)).ToList();

               result.Result.InSrcNotInDst = (from groupMemberSrc in result.Result.InSrc
                                              join groupMemberDst in result.Result.InDst on groupMemberSrc.DistinguishedName equals groupMemberDst.DistinguishedName into groupMembersDst
                                              from groupMemberDst in groupMembersDst.DefaultIfEmpty()
                                              where groupMemberDst == null
                                              select groupMemberSrc).GroupBy(memb => memb.DistinguishedName).Select(memb => memb.FirstOrDefault()).ToList();

               result.Result.InDstNotInSrc = (from groupMemberDst in result.Result.InDst
                                              join groupMemberSrc in result.Result.InSrc on groupMemberDst.DistinguishedName equals groupMemberSrc.DistinguishedName into groupMembersSrc
                                              from groupMemberSrc in groupMembersSrc.DefaultIfEmpty()
                                              where groupMemberSrc == null
                                              select groupMemberDst).GroupBy(memb => memb.DistinguishedName).Select(memb => memb.FirstOrDefault()).ToList();

               return result;
           });

            return taskResult;
        }

        public ActiveDirectorySyncController(Globals globals) : base(globals)
        {
        }
    }
}
