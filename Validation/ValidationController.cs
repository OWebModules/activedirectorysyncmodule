﻿using ActiveDirectorySyncModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActiveDirectorySyncModule.Validation
{
    public static class ValidationController
    {
        public static clsOntologyItem ValidateExportGroupMembersCSVRequest(ExportGroupMembersCSVRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (!request.GroupWithMembers.Any())
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "Nothing to export! List is empty.";
                return result;
            }

            if (string.IsNullOrEmpty(request.CSVPath))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "CSVPath is empty!";
                return result;
            }

            try
            {
                if (System.IO.File.Exists(request.CSVPath))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The file {request.CSVPath} is not existing!";
                    return result;
                }
                System.IO.File.WriteAllText(request.CSVPath, "dummy");
                System.IO.File.Delete(request.CSVPath);
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"The file {request.CSVPath} is not existing!";
                return result;

            }
            return result;
        }

        public static clsOntologyItem ValidateGetGroupMembersRequest(GetGroupMembersRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.GroupFilter))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The GroupFilter is empty!";
            }

            result = request.CheckAndSetGroupRegex(globals);

            if (result.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            if (string.IsNullOrEmpty(request.OuPrincipalName))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The principal name of the OU is empty!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateGetUserMembershipRequest(GetUserGroupsRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The IdConfig is empty!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateGetUserGroupsModel(GetUserGroupsServiceModel model, Globals globals, string propertyName = null)
        {
            var result = globals.LState_Success.Clone();

            if (propertyName == null || propertyName == nameof(GetUserGroupsServiceModel.RootConfig))
            {
                if (model.RootConfig == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Root-Config is empty!";
                    return result;
                }
            }

            if (propertyName == null || propertyName == nameof(GetUserGroupsServiceModel.ConfigsToExportPaths))
            {
                if (model.ConfigsToExportPaths.Count > 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Only one ExportPath or none is allowed!";
                    return result;
                }
            }

            if (propertyName == null || propertyName == nameof(GetUserGroupsServiceModel.ConfigsToUsers))
            {
                if (!model.ConfigsToUsers.Any())
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No users configured!";
                    return result;
                }
            }

            if (propertyName == null || propertyName == nameof(GetUserGroupsServiceModel.PrincipalPathsToUsers))
            {
                if (model.PrincipalPathsToUsers.Count != model.ConfigsToUsers.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"{model.ConfigsToUsers.Count} users are configured, but {model.PrincipalPathsToUsers.Count} PrincipalPaths are configured!";
                    return result;
                }
            }

            if (propertyName == null || propertyName == nameof(GetUserGroupsServiceModel.PrincipalNamesToUsers))
            {
                if (model.PrincipalNamesToUsers.Count != model.ConfigsToUsers.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"{model.ConfigsToUsers.Count} users are configured, but {model.PrincipalNamesToUsers.Count} PrincipalNames are configured!";
                    return result;
                }
            }

            return result;
        }
    }
}
